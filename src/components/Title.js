import React from 'react';

const Title = ({heading}) => (
    <h1>{heading}</h1>
);

export default Title;