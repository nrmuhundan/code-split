import React, { Component } from 'react';

const Error = () => (<div>Error!</div>);

class AsyncComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            Component: null,
        };
    }

    componentDidMount() {
        this.load(this.props)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.load !== this.props.load) {
            this.load(nextProps)
        }
    }

    load(props) {
        props.loader()
            .then(Component => {
                this.setState({ Component });
            })
            .catch(error => {
                this.setState({ Component: Error});
            });
    }

    render() {
        const { Component } = this.state;

        if (Component) {
            return <Component {...this.props} />;
        }

        return <div>'Loading...'</div>;
    }
}

export default AsyncComponent;
