import React from 'react';
import { Route, Redirect, Link } from 'react-router-dom'
import Title from '../Title';
import AsyncComponent from '../AsyncComponent';

const BasicAsync = props => (
    <AsyncComponent
        {...props}
        loader = {() => import(/* webpackChunkName: "basic" */ './Basic').then(c => c.default)}
    />
);

const ContactAsync = props => (
    <AsyncComponent
        {...props}
        loader = {() => import(/* webpackChunkName: "contact" */ './Contact').then(c => c.default)}
    />
);

const User = ({match}) => (
    <div>
        <Title heading="User"/>
        <Link to="/">Exit</Link>
        <hr/>
        <Link to={match.url + '/basic'}>Basic</Link>|
        <Link to={match.url + '/contact'}>Contact</Link>
        <hr/>
        <Route path={match.url + '/basic'} component={BasicAsync}/>
        <Route path={match.url + '/contact'} component={ContactAsync}/>
        <Redirect from={match.url} to={match.url + '/basic'}/>
    </div>
); 

export default User;

