import React from 'react';

const Header = (props) => (
    <h3>{props.children}</h3>
);

export default Header;