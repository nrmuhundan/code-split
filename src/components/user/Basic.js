import React from 'react';
import Header from './Header';
import { connect } from 'react-redux';
import { updateFirstName } from '../../actions';


const Basic = ({match, basic, onFirstNameChange}) => {
    const firstNameChange = event => {
        onFirstNameChange(event.target.value);
    }

    return(
        <div>
            <Header>Basic</Header>
            <label for='firstname'>Firstname: </label>
            <input id='firstname' type='text' value={basic.firstname} onChange={firstNameChange}/>
        </div>
    );
}; 

const mapStateToProps = state => ({
    basic: state.user.basic,
})

const mapDispatchToProps = dispatch => ({
   onFirstNameChange: value => {dispatch(updateFirstName(value))},
});

export default connect(mapStateToProps, mapDispatchToProps)(Basic);

