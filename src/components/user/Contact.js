import React from 'react';
import Header from './Header';
import { connect } from 'react-redux';

const Contact = ({match, contact}) => (
    <div>
        <Header>Contact</Header>
        <p>Phone: {contact.phone}</p>
    </div>
); 

const mapStateToProps = state => ({
    contact: state.user.contact,
})

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Contact);

