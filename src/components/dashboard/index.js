import React from 'react';
import { Link } from 'react-router-dom'
import Title from '../Title';

const Dashboard = () => (
    <div>
        <Title heading="Dashboard"/>
        <Link to="/123">User 123</Link>
    </div>
); 

export default Dashboard;

