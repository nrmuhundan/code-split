import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Dashboard from './components/dashboard';
import User from './components/user';
import './App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Route exact path="/" component={Dashboard}/>
          <Route path="/:id" component={User}/>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
