import { UPDATE_FIRSTNAME } from '../constants';

const updateFirstName = value => ({
    type: UPDATE_FIRSTNAME,
    value,
});

export { updateFirstName };