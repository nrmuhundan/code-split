const defaultState = [{
    id: 123,
    firstname: 'FirstName1',
    lastname: 'LastName1',
}, {
    id: 124,
    firstname: 'FirstName1',
    lastname: 'LastName1',
}];

const usersReducer = (state = defaultState) => state;
export default usersReducer; 