import { UPDATE_FIRSTNAME } from '../constants';
import { combineReducers } from 'redux'

const defaultBasicState = {
    firstname: 'FirstName1',
    lastname: 'LastName1',
};

const defaultContactState = {
    phone: '+61412341234',
}

const basicReducer = (state = defaultBasicState, action) => {
    switch(action.type){
        case UPDATE_FIRSTNAME : {
            return {
                ...state,
                firstname: action.value,
            };
        }
        default : {
            return state;
        }
    }
};

const contactReducer = (state = defaultContactState) => (state);

const userReducer = combineReducers({
    basic: basicReducer,
    contact: contactReducer,
});

export default userReducer; 