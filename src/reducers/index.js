import { combineReducers } from 'redux';
import users from './usersReducer';
import user from './userReducer';

const combined = combineReducers({
    users,
    user,
});

export default combined;
